package com.example.zach.database_test;

import java.util.UUID;

/**
 * Created by Zach on 7/14/2016.
 */
public class Item {
    private UUID mID;
    private String mName;

    public Item(String name){
        mName = name;
        mID = UUID.randomUUID();
    }

    public UUID getID() {
        return mID;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }
}
