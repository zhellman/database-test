package com.example.zach.database_test;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zach on 7/14/2016.
 */
public class MainFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private Button mAddButton;
    private ArrayList<Item> mItems;
    private ItemAdapter mAdapter;
    private EditText mEditText;
    private Button mDeleteButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_main_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mEditText = (EditText) view.findViewById(R.id.fragment_main_edit_text);

        mAddButton = (Button) view.findViewById(R.id.fragment_main_submit_button);
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //do something
                ItemRack itemRack = ItemRack.get(getActivity());
                String text = String.valueOf(mEditText.getText());
                if(text.length() != 0) {
                    itemRack.addItem(new Item(text));
                    updateUI();
                }
            }
        });

        mDeleteButton = (Button) view.findViewById(R.id.fragment_main_delete_button);
        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //do something
                ItemRack itemRack = ItemRack.get(getActivity());
                itemRack.deleteItem();
                updateUI();
            }
        });

        updateUI();

        return view;
    }//End Method

    private void updateUI() {
        ItemRack itemRack = ItemRack.get(getActivity());
        List<Item> items = itemRack.getItems();

        if(mAdapter == null) {
            mAdapter = new ItemAdapter(items);
            mRecyclerView.setAdapter(mAdapter);
        }
        else{
            mAdapter.setItems(items);
            mAdapter.notifyDataSetChanged();
        }
    }

    private List<Item> makeItems() {
            ArrayList<Item> items = new ArrayList<>();
            for(int i=0; i<20; i++) {
                items.add(new Item("Item " + i));
            }
            return items;
    }

    /*Adapter inner class*/
    private class ItemAdapter extends RecyclerView.Adapter<ItemHolder> {

        private List<Item> mItems;

        public ItemAdapter(List<Item> items) {
            mItems = items;
        }

        @Override
        public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater
                    .inflate(android.R.layout.simple_list_item_1, parent, false);
            return new ItemHolder(view);
        }

        @Override
        public void onBindViewHolder(ItemHolder holder, int position) {
            Item item = mItems.get(position);
            holder.bindItem(item);
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public void setItems(List<Item> items) {
            mItems = items;
        }
    }

    /*ViewHolder inner class*/
    private class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private Item mItem;
        private TextView mTextView;

        public ItemHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mTextView = (TextView) itemView;
        }

        public void bindItem(Item item) {
            mItem = item;
            mTextView.setText(mItem.getName());
        }

        @Override
        public void onClick(View v){
            // Do something
            ItemRack itemRack = ItemRack.get(getActivity());
            itemRack.deleteItem();
            mAdapter.notifyDataSetChanged();
        }
    }//End Inner Class
}