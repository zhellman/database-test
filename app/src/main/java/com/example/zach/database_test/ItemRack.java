package com.example.zach.database_test;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Zach on 7/14/2016.
 */
public class ItemRack {
    private static ItemRack sItemRack;
    private List<Item> mItems;

    public static ItemRack get(Context context) {
        if (sItemRack == null) {
            sItemRack = new ItemRack(context);
        }
        return sItemRack;
    }
    private ItemRack(Context context) {
        mItems = new ArrayList<>();
    }

    public void addItem(Item item) {
        mItems.add(item);
    }

    public void deleteItem() {
        if(mItems.size() > 0) {
            mItems.remove(mItems.size() - 1);
        }
    }

    public List<Item> getItems() {
        return mItems;
    }

    public Item getItem(UUID id) {
        for (Item item : mItems) {
            if (item.getID().equals(id)) {
                return item;
            }
        }
        return null;
    }// End method
} // End class